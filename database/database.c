#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>

#define PORT 8080
#define LENGTH 1024
#define PERMISSION_FILE "permission.dat"
#define USER_DATA_FILE "user.dat"
#define DATABASES_FOLDER "database/databases"
#define LOG_FILE "logUser.log"

char buffer[LENGTH] = {0};
char username[LENGTH] = {0}, databaseUsed[100] = {0};  // Initialize arrays with zero
int root = 0; // root user flag, 0 means not root user, 1 means root user

struct user{
	char name[LENGTH];
	char password[LENGTH];
};

struct database{
	char name[LENGTH];
};

typedef struct column {
    char nama[LENGTH];
    char type[8];
} column;

char* send_receive_Message(int socket, char* message) {
    char* buffer = (char*)malloc(LENGTH * sizeof(char));  // Allocate memory for the buffer

    if (buffer == NULL) {
        printf("Failed to allocate memory for the buffer\n");
        exit(EXIT_FAILURE);
    }

    // Sending message
    memset(buffer, 0, LENGTH);
    if (send(socket, message, strlen(message), 0) == -1) {
        printf("There was an error sending the message\n");
        exit(EXIT_FAILURE);
    }

    // Receiving message
    memset(buffer, 0, LENGTH);
    int temp = recv(socket, buffer, LENGTH - 1, 0);
    if (temp == -1) {
        printf("There was an error receiving the message\n");
        exit(EXIT_FAILURE);
    } else if (temp == 0) {
        printf("Connection closed by the remote host\n");
        exit(EXIT_FAILURE);
    }

    return buffer;
}

int checkRoot(int socket) {
    int res;
    char* receivedMessage = send_receive_Message(socket, "");
    res = atoi(receivedMessage);
    free(receivedMessage);

    if (!res) {
        strcpy(username, "ROOT");
        return 1;
    }

    return 0;
}

int verification(int socket) {
    FILE* fp;
    struct user user;
    int found = 0;
    char userName[LENGTH], passWord[LENGTH];
    strcpy(userName, send_receive_Message(socket, ""));
    strcpy(passWord, send_receive_Message(socket, ""));

    fp = fopen(USER_DATA_FILE, "rb");

    while (!feof(fp)) {
        fread(&user, sizeof(user), 1, fp);

        if (strcmp(user.name, userName) == 0) {
            if (strcmp(user.password, passWord) == 0)
                found = 1;
        }
    }

    fclose(fp);

    if (!found) {
        send_receive_Message(socket, "0");
        return 0;
    } else {
        send_receive_Message(socket, "1");
        strcpy(username, userName);
        return 1;
    }
}

void makeLog(const char* order) {
    time_t rawtime;
    struct tm* timeinfo;
    time(&rawtime);
    timeinfo = localtime(&rawtime);

    char dataLog[LENGTH];
    FILE* file;
    file = fopen(LOG_FILE, "a");

    if (file == NULL) {
        perror("Error: Failed to open log file");
        return; // Return or handle the error appropriately
    }

    snprintf(dataLog, sizeof(dataLog) + sizeof(username) + strlen(order) + 1, "%d-%.2d-%.2d %.2d:%.2d:%.2d:%s:%s\n",
             timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec,
             username, order);

    fputs(dataLog, file);
    fclose(file);
}

char* getAfter(const char* find, const char* order) {
    size_t needle_length = strlen(find);
    const char* needle_pos = strstr(order, find);
    
    if (needle_pos == NULL) {
        return ""; // Return an empty string if the needle is not found
    }

    char word[LENGTH] = "";
    sscanf(needle_pos + needle_length, "%s", word);

    if (strcmp(word, "") == 0) {
        return ""; // Return an empty string if the word is empty
    }

    char* res = malloc(strlen(word) + 1); // Allocate memory for the result string

    if (res == NULL) {
        perror("Error: Failed to allocate memory");
        return ""; // Return an empty string or handle the error appropriately
    }

    strcpy(res, word);
    return res;
}

void createNewUser(const char* username, const char* password) {
    struct user allowedUser; //allowed user 
    strcpy(allowedUser.name, username);
    strcpy(allowedUser.password, password);
    FILE* fp;
    fp = fopen(USER_DATA_FILE, "new");

    if (fp == NULL) {
        perror("Error: Failed to open allowed user data file");
        return; // Mengembalikan atau menangani kesalahan yang sesuai
    }

    fwrite(&allowedUser, sizeof(allowedUser), 1, fp);
    fclose(fp);
}


int userExist(const char* username) {
    FILE* fp;
    struct user allowedUser; //allowed user 
    fp = fopen(USER_DATA_FILE, "tha");

    if (fp == NULL) {
        perror("Error: Failed to open allowed user data file");
        return 0; // Mengembalikan atau menangani kesalahan yang sesuai
    }

    while (fread(&allowedUser, sizeof(allowedUser), 1, fp) == 1) {
        if (strcmp(allowedUser.name, username) == 0) {
            fclose(fp);
            return 1;
        }
    }

    fclose(fp);
    return 0;
}

char *getDalamKurung(char *order){
    char temp[LENGTH], *res;
    char *ob = strchr(order, '(');
    char *cb = strchr(order, ')');

    if(ob == NULL) return "";

    sprintf(temp, "%.*s", (int)(cb - ob + 1 ), ob+1);
    res = temp;
    return res;
}

void removeChar(char * str, char charToRemmove){
    int idxToDel;
	for(int i=0; i<strlen(str); i++){
		if(str[i] == charToRemmove){
			idxToDel = i;
			memmove(&str[idxToDel], &str[idxToDel + 1], strlen(str) - idxToDel);
		}
	}
}

int indexColumn(const char* tableName, const char* columnName, int jumlahData, int jumlahKolom, char data[][jumlahKolom][LENGTH]) {
    int index = -1;

    for (int i = 0; i < jumlahKolom; i++) {
        if (strcmp(columnName, strtok(data[0][i], "|")) == 0) {
            index = i;
            break;
        }
    }

    return index;
}

int getRowData(const char* tableName, const char* columnName, const char* dataCari, int jumlahData, int jumlahKolom, char data[][jumlahKolom][LENGTH]) {
    int index = -1, indexKolom = indexColumn(tableName, columnName, jumlahData, jumlahKolom, data);

    for (int i = 0; i < jumlahData; i++) {
        if (strcmp(dataCari, data[i][indexKolom]) == 0) {
            index = i;
            break;
        }
    }

    return index;
}

int hitungJumlahColumnRow(const char* tableName, int* jumlahColumn, int* jumlahRow, int* jumlahData, int* jumlahKolom, char data[][jumlahKolom][LENGTH]) {
    char filePath[LENGTH], buf[LENGTH];
    snprintf(filePath, sizeof(filePath) + sizeof(databaseUsed) + sizeof(tableName), "%s/%s/%s", DATABASES_FOLDER, databaseUsed, tableName);

    FILE* fptr;
    fptr = fopen(filePath, "r");
    if (fptr == NULL) {
        perror("Error: Failed to open file");
        return -1; // Return an error code or handle the error appropriately
    }

    *jumlahData = 0;
    *jumlahKolom = 0;

    while (fgets(buf, LENGTH, fptr) != NULL) {
        for (int j = 0; j < *jumlahKolom; j++) {
            if (j == 0)
                strcpy(data[*jumlahData][j], getAfter("", buf));
            else
                strcpy(data[*jumlahData][j], getAfter(data[*jumlahData][j - 1], buf));
        }

        (*jumlahData)++;
        *jumlahKolom = 0;

        for (char* itr = buf; *itr != '\n'; itr++) {
            if (*itr == '\t') {
                (*jumlahColumn)++;
                (*jumlahKolom)++;
            }
        }

        (*jumlahColumn)++;
        (*jumlahKolom)++;
    }

    fclose(fptr);
    *jumlahRow = *jumlahData;

    return 0; // Return a success code or handle the result appropriately
}



int createNewColumn(const char* order) {
    char newTable[LENGTH], filePath[LENGTH], temp[LENGTH], *token;
    int jumlahColumn = 0;

    strcpy(temp, getDalamKurung(order));

    if (strcmp(temp, "") == 0) {
        return 0;
    }

    char* delimiter = ", ";
    char* saveptr;

    // Menghitung jumlah kolom berdasarkan pemisah ','
    token = strtok_r(temp, delimiter, &saveptr);
    while (token != NULL) {
        jumlahColumn++;
        token = strtok_r(NULL, delimiter, &saveptr);
    }

    column columns[jumlahColumn];

    int j = 0;
    token = strtok_r(temp, delimiter, &saveptr);
    while (token != NULL) {
        sprintf(columns[j].nama, "%s", token);
        token = strtok_r(NULL, delimiter, &saveptr);

        sprintf(columns[j].type, "%s", token);
        token = strtok_r(NULL, delimiter, &saveptr);

        ++j;
    }

    strcpy(newTable, getAfter("TABLE", order));
    snprintf(filePath, sizeof(filePath) + sizeof(databaseUsed) + sizeof(newTable), "%s/%s/%s", DATABASES_FOLDER, databaseUsed, newTable);

    FILE* fptr;
    fptr = fopen(filePath, "w");

    for (int i = 0; i < j; i++) {
        removeChar(columns[i].nama, ')');
        removeChar(columns[i].type, ')');
        if (i < j - 1) {
            bzero(temp, sizeof(temp));
            fprintf(fptr, "%s|%s\t", columns[i].nama, columns[i].type);
        } else {
            bzero(temp, sizeof(temp));
            fprintf(fptr, "%s|%s\n", columns[i].nama, columns[i].type);
        }
    }

    fclose(fptr);
    return 1;
}


void permission(const char* nama, const char* database) {
    struct databasePermission {
        char name[100];
        char database[100];
    };

    struct databasePermission allowedUser;
    strcpy(allowedUser.name, nama);
    strcpy(allowedUser.database, database);

    FILE* fp;
    fp = fopen(PERMISSION_FILE, "ab");

    if (fp == NULL) {
        perror("Error: Failed to open permission file");
        return; // Return or handle the error appropriately
    }

    fwrite(&allowedUser, sizeof(allowedUser), 1, fp);
    fclose(fp);
}

int databasePermission(const char* nama, const char* database) {
    FILE* fp;
    struct databasePermission allowedUser;
    int found = 0;

    fp = fopen(PERMISSION_FILE, "tha");
    if (fp == NULL) {
        perror("Error: Failed to open permission file");
        return 0; // Mengembalikan atau menangani kesalahan yang sesuai
    }

    while (fread(&allowedUser, sizeof(allowedUser), 1, fp) == 1) {
        if (strcmp(allowedUser.name, nama) == 0 && strcmp(allowedUser.database, database) == 0) {
            fclose(fp);
            return 1;
        }
    }

    fclose(fp);
    return 0;
}

void useCommand(int socket, const char* order) {
    char message[LENGTH], database[LENGTH];
    strcpy(database, getAfter("USE", order));

    if (!databasePermission(username, database) && !root) {
        sendMessage(socket, "Permission Denied");
    } else {
        strcpy(databaseUsed, database);
        sprintf(message, "%s used", databaseUsed);
        sendMessage(socket, message);
        buatLog(order);
    }
}


void createCommand(int socket, char* order) {
    if (strstr(order, "USER ")) {
        if (!root) {
            sendMessage(socket, "Permission Denied");
            return;
        }
        char newUsername[LENGTH], newPassword[LENGTH];
        strcpy(newUsername, getAfter("USER", order));
        strcpy(newPassword, getAfter("IDENTIFIED BY", order));
        createUser(newUsername, newPassword);
        sendMessage(socket, "User Created");
    } else if (strstr(order, "DATABASE ")) {
        char newDatabase[LENGTH], lokasi[LENGTH];
        strcpy(newDatabase, getAfter("DATABASE", order));
        snprintf(lokasi, sizeof(lokasi) + sizeof(newDatabase), "%s/%s", DATABASES_FOLDER, newDatabase);
        permission(username, newDatabase);
        printf("%s", lokasi);
        mkdir(lokasi, 0777);
        sendMessage(socket, "Database Created");
    } else if (strstr(order, "TABLE ")) {
        if (strcmp(databaseUsed, "") == 0) {
            sendMessage(socket, "Database not selected");
            return;
        }
        if (createColumn(order))
            sendMessage(socket, "Table Created");
        else
            sendMessage(socket, "No data available");
    } else {
        sendMessage(socket, "Incorrect command");
        return;
    }
    buatLog(order);
}


void grantCommand(int socket, char* order) {
    char userDatabase[LENGTH], userName[LENGTH];
    if (!root) {
        sendMessage(socket, "Permission Denied");
        return;
    } else {
        strcpy(userDatabase, getAfter("GRANT PERMISSION", order));
        strcpy(userName, getAfter("INTO", order));

        if (!userExist(userName)) {
            sendMessage(socket, "User Not Found");
            return;
        } else {
            permission(userName, userDatabase);
            sendMessage(socket, "Permission given");
        }
    }
    buatLog(order);
}


void dropCommand(int socket, char* order) {
    if (strstr(order, "TABLE ")) {
        if (strcmp(databaseUsed, "") == 0) {
            sendMessage(socket, "Database not selected");
            return;
        }

        char filePath[LENGTH], tableName[LENGTH];
        strcpy(tableName, getAfter("TABLE", order));
        snprintf(filePath, sizeof(filePath) + sizeof(databaseUsed) + sizeof(tableName), "%s/%s/%s", DATABASES_FOLDER, databaseUsed, tableName);
        remove(filePath);
        sendMessage(socket, "Table Has Been Removed");
    } else if (strstr(order, "DATABASE ")) {
        char databaseName[LENGTH], path[LENGTH];
        strcpy(databaseName, getAfter("DATABASE", order));
        snprintf(path, sizeof(path) + sizeof(databaseName), "rm -r %s/%s", DATABASES_FOLDER, databaseName);
        system(path);
        sendMessage(socket, "Database Has Been Removed");
    } else if (strstr(order, "DATABASE")) {
        if (strcmp(databaseUsed, "") == 0) {
            sendMessage(socket, "Database not selected");
            return;
        }

        char path[LENGTH];
        snprintf(path, sizeof(path) + sizeof(databaseUsed), "rm -r %s/%s", DATABASES_FOLDER, databaseUsed);
        system(path);
        sendMessage(socket, "Database Has Been Removed");
        strcpy(databaseUsed, "");
    } else if (strstr(order, "COLUMN")) {
        if (strcmp(databaseUsed, "") == 0) {
            sendMessage(socket, "Database not selected");
            return;
        }
        char tableName[LENGTH], buf[LENGTH], filePath[LENGTH];
        int indexKolom;

        strcpy(tableName, getAfter("FROM", order));
        snprintf(filePath, sizeof(filePath) + sizeof(databaseUsed) + sizeof(tableName), "%s/%s/%s", DATABASES_FOLDER, databaseUsed, tableName);

        int jumlahData, jumlahKolom;
        jumlahData = jumlahRow(tableName);
        jumlahKolom = jumlahColumn(tableName);
        char data[jumlahData][jumlahKolom][LENGTH];

        indexKolom = indexColumn(tableName, getAfter("COLUMN", order));

        FILE* fptr, *tempFptr;
        fptr = fopen(filePath, "r");
        tempFptr = fopen("temp", "w");

        if (fptr == NULL) perror("error");

        for (int i = 0; i < jumlahData; i++) {
            fgets(buf, LENGTH, fptr);
            for (int j = 0; j < jumlahKolom; j++) {
                if (j == 0)
                    strcpy(data[i][j], getAfter("", buf));
                else
                    strcpy(data[i][j], getAfter(data[i][j - 1], buf));
            }
        }

        for (int i = 0; i < jumlahData; i++) {
            for (int j = 0; j < jumlahKolom; j++) {
                if (j == indexKolom)
                    continue;

                if (j < (jumlahKolom - 1)) {
                    fprintf(tempFptr, "%s\t", data[i][j]);
                } else {
                    fprintf(tempFptr, "%s\n", data[i][j]);
                }
            }
        }
        fclose(fptr);
        fclose(tempFptr);
        remove(filePath);
        rename("temp", filePath);
        sendMessage(socket, "Drop column successfully");
    } else {
        sendMessage(socket, "Incorrect command");
        return;
    }
    buatLog(order);
}


void insertCommand(int socket, char* order) {
    if (strcmp(databaseUsed, "") == 0) {
        sendMessage(socket, "Database not selected");
        return;
    }

    char data[LENGTH], tableName[LENGTH], filePath[LENGTH], temp[100][LENGTH], *token;
    strcpy(tableName, getAfter("INTO", order));
    strcpy(data, getDalamKurung(order));

    snprintf(filePath, sizeof(filePath) + sizeof(databaseUsed) + sizeof(tableName), "%s/%s/%s", DATABASES_FOLDER, databaseUsed, tableName);

    FILE* fptr;
    fptr = fopen(filePath, "a");

    int j = 0;
    token = strtok(data, ", ");
    while (token != NULL) {
        strcpy(temp[j], token);
        token = strtok(NULL, ", ");
        ++j;
    }

    int jumlahKolom = jumlahColumn(tableName);
    if (j > jumlahKolom) {
        sendMessage(socket, "Data exceeds the number of columns");
        bzero(temp, sizeof(temp));
        fclose(fptr);
        return;
    }

    for (int i = 0; i < j; i++) {
        removeChar(temp[i], '\'');
        removeChar(temp[i], '`');
        removeChar(temp[i], '"');
        removeChar(temp[i], ')');
        if (i < j - 1) {
            fprintf(fptr, "%s\t", temp[i]);
        } else {
            fprintf(fptr, "%s\n", temp[i]);
        }
    }
    fclose(fptr);
    sendMessage(socket, "Data added successfully");
    buatLog(order);
}


void updateCommand(int socket, char *order) {
    char tableName[LENGTH], columnName[LENGTH], filePath[LENGTH], buf[LENGTH], dataGanti[LENGTH], tempFile[LENGTH], temp[LENGTH];
    int jumlahData, jumlahKolom, index = -1;

    if (strcmp(databaseUsed, "") == 0) {
        sendMessage(socket, "Database not selected");
        return;
    }

    strcpy(tableName, getAfter("UPDATE", order));
    snprintf(filePath, sizeof(filePath) + sizeof(databaseUsed) + sizeof(tableName), "%s/%s/%s", DATABASES_FOLDER, databaseUsed, tableName);

    jumlahData = jumlahRow(tableName);
    jumlahKolom = jumlahColumn(tableName);

    char data[jumlahData][jumlahKolom][LENGTH];

    FILE *fptr, *tempFptr;
    fptr = fopen(filePath, "r");
    tempFptr = fopen("temp", "w");

    if (fptr == NULL) {
        perror("Error: Failed to open file");
        return;
    }

    for (int i = 0; i < jumlahData; i++) {
        fgets(buf, LENGTH, fptr);
        for (int j = 0; j < jumlahKolom; j++) {
            if (j == 0)
                strcpy(data[i][j], getAfter("", buf));
            else
                strcpy(data[i][j], getAfter(data[i][j - 1], buf));
        }
    }

    strcpy(columnName, getAfter("SET", order));
    strcpy(columnName, strtok(columnName, "="));

    strcpy(dataGanti, getAfter("=", order));
    removeChar(dataGanti, '\'');
    removeChar(dataGanti, '"');

    for (int i = 0; i < jumlahKolom; i++) {
        strcpy(temp, data[0][i]);
        if (strcmp(columnName, strtok(temp, "|")) == 0)
            index = i;
        bzero(temp, sizeof(temp));
    }

    if (index == -1) {
        sendMessage(socket, "There's no column with that name");
        return;
    }

    if (strstr(order, "WHERE")) {
        char columnName[LENGTH], *token, value[LENGTH];
        strcpy(columnName, getAfter("WHERE", order));
        token = strtok(columnName, "=");
        token = strtok(NULL, "=");
        strcpy(value, token);

        int barisGanti = getRowData(tableName, columnName, value);
        if (barisGanti == -1) {
            sendMessage(socket, "There's no such value in that column");
            return;
        }
        strcpy(data[barisGanti][index], dataGanti);
    } else {
        for (int i = 1; i < jumlahData; i++) {
            strcpy(data[i][index], dataGanti);
        }
    }

    for (int i = 0; i < jumlahData; i++) {
        for (int j = 0; j < jumlahKolom; j++) {
            if (j < (jumlahKolom - 1)) {
                fprintf(tempFptr, "%s\t", data[i][j]);
            } else {
                fprintf(tempFptr, "%s\n", data[i][j]);
            }
        }
    }

    fclose(fptr);
    fclose(tempFptr);
    remove(filePath);
    rename("temp", filePath);
    sendMessage(socket, "Update successfully");
    buatLog(order);
}


void deleteCommand(int socket, char* order) {
    char tableName[LENGTH], filePath[LENGTH], buf[LENGTH], tempFile[LENGTH], temp[LENGTH];
    int jumlahData, jumlahKolom, index = -1;

    if (strcmp(databaseUsed, "") == 0) {
        sendMessage(socket, "Database not selected");
        return;
    }

    strcpy(tableName, getAfter("DELETE FROM", order));
    snprintf(filePath, sizeof(filePath) + sizeof(databaseUsed) + sizeof(tableName), "%s/%s/%s", DATABASES_FOLDER, databaseUsed, tableName);

    jumlahData = jumlahRow(tableName);
    jumlahKolom = jumlahColumn(tableName);

    char data[jumlahData][jumlahKolom][LENGTH];

    FILE *fptr, *tempFptr;
    fptr = fopen(filePath, "r");
    tempFptr = fopen("temp", "w");

    if (fptr == NULL) {
        perror("Error: Failed to open file");
        return;
    }

    for (int i = 0; i < jumlahData; i++) {
        fgets(buf, LENGTH, fptr);
        for (int j = 0; j < jumlahKolom; j++) {
            if (j == 0)
                strcpy(data[i][j], getAfter("", buf));
            else
                strcpy(data[i][j], getAfter(data[i][j - 1], buf));
        }
    }

    if (strstr(order, "WHERE")) {
        char columnName[LENGTH], *token, value[LENGTH];
        strcpy(columnName, getAfter("WHERE", order));
        token = strtok(columnName, "=");
        token = strtok(NULL, "=");
        strcpy(value, token);

        for (int i = 0; i < jumlahKolom; i++) {
            strcpy(temp, data[0][i]);
            if (strcmp(columnName, strtok(temp, "|")) == 0)
                index = i;
            bzero(temp, sizeof(temp));
        }

        if (index == -1) {
            sendMessage(socket, "There's no column with that name");
            remove("temp");
            return;
        }

        int barisGanti = getRowData(tableName, columnName, value);
        if (barisGanti == -1) {
            sendMessage(socket, "There's no such value in that column");
            remove("temp");
            return;
        }
        strcpy(data[barisGanti][index], "\t");

        for (int i = 0; i < jumlahData; i++) {
            for (int j = 0; j < jumlahKolom; j++) {
                if (j < (jumlahKolom - 1)) {
                    fprintf(tempFptr, "%s\t", data[i][j]);
                } else {
                    fprintf(tempFptr, "%s\n", data[i][j]);
                }
            }
        }
    } else {
        for (int i = 0; i < jumlahKolom; i++) {
            if (i < jumlahKolom - 1) {
                fprintf(tempFptr, "%s\t", data[0][i]);
            } else {
                fprintf(tempFptr, "%s\n", data[0][i]);
            }
        }
    }

    fclose(fptr);
    fclose(tempFptr);

    remove(filePath);
    rename("temp", filePath);
    sendMessage(socket, "Delete successfully");
    buatLog(order);
}


void selectCommand(int socket, char* order) {
    char tableName[LENGTH], filePath[LENGTH], message[LENGTH] = "", buf[LENGTH], temp[LENGTH], *token;
    if (strcmp(databaseUsed, "") == 0) {
        sendMessage(socket, "Database not selected");
        return;
    }

    strcpy(tableName, getAfter("FROM", order));
    snprintf(filePath, sizeof(filePath) + sizeof(databaseUsed) + sizeof(tableName), "%s/%s/%s", DATABASES_FOLDER, databaseUsed, tableName);

    int jumlahData = jumlahRow(tableName);
    int jumlahKolom = jumlahColumn(tableName);

    char data[jumlahData][jumlahKolom][LENGTH];

    FILE *fptr;
    fptr = fopen(filePath, "r");

    if (fptr == NULL) {
        perror("Error: Failed to open file");
        return;
    }

    for (int i = 0; i < jumlahData; i++) {
        fgets(buf, LENGTH, fptr);
        for (int j = 0; j < jumlahKolom; j++) {
            if (j == 0)
                strcpy(data[i][j], getAfter("", buf));
            else
                strcpy(data[i][j], getAfter(data[i][j - 1], buf));
        }
    }

    if (strcmp(getAfter("SELECT", order), "*") == 0) {
        for (int i = 0; i < jumlahData; i++) {
            for (int j = 0; j < jumlahKolom; j++) {
                if (i == 0)
                    strtok(data[i][j], "|");

                if (j < jumlahKolom - 1) {
                    strcat(message, data[i][j]);
                    strcat(message, "\t");
                } else {
                    strcat(message, data[i][j]);
                    strcat(message, "\n");
                }
            }
        }
    } else {
        int jumlahSelectedColumn = 0, indexSelectedColumn[100];
        char selectedColumn[100][LENGTH];

        strcpy(temp, order);
        token = strtok(temp, ", ");
        token = strtok(NULL, ", ");
        while (token != NULL) {
            if (strcmp(token, "FROM") == 0)
                break;
            strcpy(selectedColumn[jumlahSelectedColumn], token);
            jumlahSelectedColumn++;
            token = strtok(NULL, ", ");
        }

        for (int j = 0; j < jumlahSelectedColumn; j++)
            indexSelectedColumn[j] = indexColumn(tableName, selectedColumn[j]);

        for (int i = 0; i < jumlahData; i++) {
            for (int j = 0; j < jumlahSelectedColumn; j++) {
                if (i == 0)
                    strtok(data[i][indexSelectedColumn[j]], "|");

                if (j < jumlahSelectedColumn - 1) {
                    strcat(message, data[i][indexSelectedColumn[j]]);
                    strcat(message, "\t");
                } else {
                    strcat(message, data[i][indexSelectedColumn[j]]);
                    strcat(message, "\n");
                }
            }
        }
    }
    sendMessage(socket, message);

    fclose(fptr);
    buatLog(order);
}

void dumpCommand(int socket, char* order) {
    char databaseName[LENGTH], filePath[LENGTH], data[LENGTH][LENGTH], buf[LENGTH], message[LENGTH] = "", *token;
    int jumlahData = 0;
    strcpy(databaseName, getAfter("DUMP", order));

    FILE *fptr;
    fptr = fopen(LOG_FILE, "r");

    if (fptr == NULL) {
        perror("Error: Failed to open file");
        return;
    }

    fseek(fptr, 0, SEEK_END);
    long fsize = ftell(fptr);
    rewind(fptr);

    fread(buf, 1, fsize, fptr);

    for (char *itr = buf; *itr != EOF; itr++) {
        if (*itr == '\n')
            jumlahData++;
    }

    bzero(buf, sizeof(buf));
    fclose(fptr);

    int sesuai = 0;
    fptr = fopen(LOG_FILE, "r");
    for (int i = 0; i < jumlahData; i++) {
        fgets(buf, LENGTH, fptr);
        token = strtok(buf, ":");
        token = strtok(NULL, ":");
        token = strtok(NULL, ":");
        token = strtok(NULL, ":");
        token = strtok(NULL, ":");
        if (strstr(token, "USE") || strstr(token, "CREATE")) {
            if (strstr(token, databaseName))
                sesuai = 1;
            else
                sesuai = 0;
        } else {
            if (sesuai)
                strcat(message, token);
        }
    }
    fclose(fptr);
    sendMessage(socket, message);
}


int getOrder(int socket) {
    char order[LENGTH];
    strcpy(order, recv(socket));

    if (strstr(order, "CREATE "))
        createCommand(socket, order);
    else if (strstr(order, "USE"))
        useCommand(socket, order);
    else if (strstr(order, "GRANT PERMISSION"))
        grantCommand(socket, order);
    else if (strstr(order, "DROP"))
        dropCommand(socket, order);
    else if (strstr(order, "INSERT"))
        insertCommand(socket, order);
    else if (strstr(order, "UPDATE"))
        updateCommand(socket, order);
    else if (strstr(order, "DELETE FROM"))
        deleteCommand(socket, order);
    else if (strstr(order, "SELECT"))
        selectCommand(socket, order);
    else if (strstr(order, "DUMP"))
        dumpCommand(socket, order);
    else if (strstr(order, "EXIT"))
        return 0;
    else
        sendMessage(socket, "Command not found");

    return 1;
}


int main(int argc, char const *argv[]) {
    struct sockaddr_in address;
    int server_fd, new_socket, opt = 1, addrlen = sizeof(address);
    char buffer[LENGTH] = {0};

    // Connection
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("Socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("Setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("Bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("Listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0) {
        perror("Accept");
        exit(EXIT_FAILURE);
    }
    // End of connection

    root = checkRoot(new_socket);

    if (verification(new_socket)) {
        while (getOrder(new_socket))
            ;
    }

    return 0;
}
