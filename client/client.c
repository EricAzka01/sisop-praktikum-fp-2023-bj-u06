#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mysql/mysql.h>

#define DB_HOST "localhost"
#define DB_USER "root"
#define DB_PASSWORD "root_password"
#define DB_NAME "nama_database"

void authenticateUser(const char* username, const char* password) {
    MYSQL* conn;
    MYSQL_RES* res;
    MYSQL_ROW row;

    conn = mysql_init(NULL);

    if (!mysql_real_connect(conn, DB_HOST, DB_USER, DB_PASSWORD, DB_NAME, 0, NULL, 0)) {
        fprintf(stderr, "Error connecting to database: %s\n", mysql_error(conn));
        return;
    }

    char query[256];
    snprintf(query, sizeof(query), "SELECT username, password, akses_database FROM pengguna WHERE username = '%s'", username);

    if (mysql_query(conn, query)) {
        fprintf(stderr, "Error executing query: %s\n", mysql_error(conn));
        mysql_close(conn);
        return;
    }

    res = mysql_use_result(conn);

    if ((row = mysql_fetch_row(res))) {
        if (strcmp(row[1], password) == 0) {
            printf("Authentication successful.\n");
            printf("User access: %s\n", row[2]);
        } else {
            printf("Authentication failed. Incorrect username or password.\n");
        }
    } else {
        printf("User not found.\n");
    }

    mysql_free_result(res);
    mysql_close(conn);
}

int main() {
    char username[256];
    char password[256];

    printf("Username: ");
    fgets(username, sizeof(username), stdin);
    username[strcspn(username, "\n")] = '\0';

    printf("Password: ");
    fgets(password, sizeof(password), stdin);
    password[strcspn(password, "\n")] = '\0';

    authenticateUser(username, password);

    return 0;
}

