# sisop-praktikum-fp-2023-BJ-U06

Group Members:


1. Eric Azka Nugroho [5025211064]
2. Kirana Alivia Enrico [5025211190]
3. Talitha Hayyinas Sahala [5025211263]


# I Containerization

**Question**

Buatlah Dockerfile yang berisi semua langkah yang diperlukan untuk setup environment dan menentukan bagaimana aplikasi harus dijalankan. Setelah Dockerfile berhasil dibuat, langkah selanjutnya adalah membuat Docker Image. 

Gunakan Docker CLI untuk mem-build image dari Dockerfile kalian. Setelah berhasil membuat image, verifikasi bahwa image tersebut berfungsi seperti yang diharapkan dengan menjalankan Docker Container dan memeriksa output-nya.

Publish Docker Image sistem ke Docker Hub. Output dari pekerjaan ini adalah file Docker kalian bisa dilihat secara public pada https://hub.docker.com/r/{Username}/storage-app.

Untuk memastikan sistem kalian mampu menangani peningkatan penggunaan, kalian memutuskan untuk menerapkan skala pada layanan menggunakan Docker Compose dengan instance sebanyak 5. Buat folder terpisah bernama Sukolilo, Keputih, Gebang, Mulyos, dan Semolowaru dan jalankan Docker Compose di sana.



**Answer**


For this task we were tasked to create a Dockerfile and upload it into our docker hub. Then, we need to create 5 instances (Docker Compose) that were seperated by 5 folders, which are Keputih, Gebang, Mulyos, and Semolowaru.


- Create a Dockerfile in linux and fill in the file by the following

```
# Base image
FROM python:3.9-slim

# Set working directory
WORKDIR /app

# Copy requirements file
COPY requirements.txt .

# Install dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Copy application files
COPY app.py .

# Set environment variables (jika diperlukan)
ENV APP_HOST=0.0.0.0
ENV APP_PORT=8000

# Expose port
EXPOSE 8000

# Run the application
CMD ["python", "app.py"]

```

In this example, I'll be using the version of python because we are using a python application to run this test. Before we actually can run this Dockerfile, we need to create app.py (python app and requirements.txt)

For this example, our app.py codes looks like this

```
from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello():
    return 'Hello, Docker!'

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)

```

While the requirements.txt file 

```
flask

```


Then, after creating both of this file, we can execute the docker file by building the docker file ```sudo docker build -t storage-app .``` After it has successfully building the docker file, we can run it by using this command ```sudo docker run -p 8000:8000 storage-app```

After it has successfully run, we can upload it into our docker hub, which we just push the docker file ```sudo docker push ericazka/storage-app```.

Then, for creating the docker compose, we need to create the file (docker compose) and fill the file with the following codes

```
version: '3'
services:
  sukolilo:
    build:
      context: ./Sukolilo
      dockerfile: ../Dockerfile
    ports:
      - '8000'
    environment:
      - APP_HOST=0.0.0.0
      - APP_PORT=8000

  keputih:
    build:
      context: ./Keputih
      dockerfile: ../Dockerfile
    ports:
      - '8001'
    environment:
      - APP_HOST=0.0.0.0
      - APP_PORT=8001

  gebang:
    build:
      context: ./Gebang
      dockerfile: ../Dockerfile
    ports:
      - '8002'
    environment:
      - APP_HOST=0.0.0.0
      - APP_PORT=8002

  mulyos:
    build:
      context: ./Mulyos
      dockerfile: ../Dockerfile
    ports:
      - '8003'
    environment:
      - APP_HOST=0.0.0.0
      - APP_PORT=8003

  semolowaru:
    build:
      context: ./Semolowaru
      dockerfile: ../Dockerfile
    ports:
      - '8004'
    environment:
      - APP_HOST=0.0.0.0
      - APP_PORT=8004

```

After creating the docker compose, run it by using this following command ``` docker-compose up --scale sukolilo=1 --scale keputih=1 --scale gebang=1 --scale mulyos=1 --scale semolowaru=1 ```

# A-E Containerization
**Database.c**

```
#define PORT 8080
#define LENGTH 1024
#define PERMISSION_FILE "permission.dat"
#define USER_DATA_FILE "user.dat"
#define DATABASES_FOLDER "database/databases"
#define LOG_FILE "logUser.log"
```
The first constant, ``PORT``, is set to the value 8080 and likely represents the port number used for network communication.
The ``LENGTH`` constant is assigned the value 1024, which could indicate a buffer size or a maximum length for data storage.
The next four constants are defined as strings. ``PERMISSION_FILE`` is set to "permission.dat", suggesting it refers to a file that stores permission-related information. ``USER_DATA_FILE`` is assigned the value "user.dat" and probably represents a file where user data is stored. ``DATABASES_FOLDER`` is set to "database/databases" and likely represents the path or folder location where database files are stored. Lastly, ``LOG_FILE`` is set to "logUser.log" and likely represents the file name for a log file that records user-related activities or events.
```
char buffer[LENGTH] = {0};
char username[LENGTH] = {0}, databaseUsed[100] = {0};  // Initialize arrays with zero
int root = 0; // root user flag, 0 means not root user, 1 means root user
```
The array is initialized with all elements set to 0. This array is likely used to store data or messages.The next line declares three character arrays: username, databaseUsed, and root. Each array is also initialized with all elements set to 0. 

```
struct user{
	char name[LENGTH];
	char password[LENGTH];
};

struct database{
	char name[LENGTH];
};

typedef struct column {
    char nama[LENGTH];
    char type[8];
} column;

```
These structures provide a way to organize and store related data in a program. They define the layout and types of data that can be stored in variables of these structures. The typedef for ``column`` allows the use of ``column`` as a type name without having to explicitly write ``struct column`` every time a variable of that type is declared.

```
char* send_receive_Message(int socket, char* message) {
    char* buffer = (char*)malloc(LENGTH * sizeof(char));  // Allocate memory for the buffer

    if (buffer == NULL) {
        printf("Failed to allocate memory for the buffer\n");
        exit(EXIT_FAILURE);
    }

    // Sending message
    memset(buffer, 0, LENGTH);
    if (send(socket, message, strlen(message), 0) == -1) {
        printf("There was an error sending the message\n");
        exit(EXIT_FAILURE);
    }

    // Receiving message
    memset(buffer, 0, LENGTH);
    int temp = recv(socket, buffer, LENGTH - 1, 0);
    if (temp == -1) {
        printf("There was an error receiving the message\n");
        exit(EXIT_FAILURE);
    } else if (temp == 0) {
        printf("Connection closed by the remote host\n");
        exit(EXIT_FAILURE);
    }

    return buffer;
}

```
This function is responsible for sending the `message` over the provided `socket` and receiving a response.
```
int checkRoot(int socket) {
    int res;
    char* receivedMessage = send_receive_Message(socket, "");
    res = atoi(receivedMessage);
    free(receivedMessage);

    if (!res) {
        strcpy(username, "ROOT");
        return 1;
    }

    return 0;
}

```
The `checkRoot` function is a straightforward function that checks if the user associated with the given `socket` is a root user. It returns an integer value indicating the result: 1 if the user is a root user and 0 otherwise.

```
int verification(int socket) {
    FILE* fp;
    struct user user;
    int found = 0;
    char userName[LENGTH], passWord[LENGTH];
    strcpy(userName, send_receive_Message(socket, ""));
    strcpy(passWord, send_receive_Message(socket, ""));

    fp = fopen(USER_DATA_FILE, "rb");

    while (!feof(fp)) {
        fread(&user, sizeof(user), 1, fp);

        if (strcmp(user.name, userName) == 0) {
            if (strcmp(user.password, passWord) == 0)
                found = 1;
        }
    }

    fclose(fp);

    if (!found) {
        send_receive_Message(socket, "0");
        return 0;
    } else {
        send_receive_Message(socket, "1");
        strcpy(username, userName);
        return 1;
    }
}
```
This `verification` function performs user verification based on the provided `socket` connection. It returns an integer value indicating the result of the verification: 1 if the user is verified successfully, and 0 otherwise.Inside the function, a file pointer `fp` is declared to handle file operations. A structure `user` is defined to hold user information. 
```
void makeLog(const char* order) {
    time_t rawtime;
    struct tm* timeinfo;
    time(&rawtime);
    timeinfo = localtime(&rawtime);

    char dataLog[LENGTH];
    FILE* file;
    file = fopen(LOG_FILE, "a");

    if (file == NULL) {
        perror("Error: Failed to open log file");
        return; // Return or handle the error appropriately
    }

    snprintf(dataLog, sizeof(dataLog) + sizeof(username) + strlen(order) + 1, "%d-%.2d-%.2d %.2d:%.2d:%.2d:%s:%s\n",
             timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec,
             username, order);

    fputs(dataLog, file);
    fclose(file);
}
```
The `makeLog` function is responsible for creating a log entry based on the provided `order` string. It writes the log entry to a file specified by `LOG_FILE`.The `username` global variable is assumed to be defined elsewhere and accessible within this function. The function appends the log entry to the file each time it is called, making it suitable for creating a chronological log of events.
```
char* getAfter(const char* find, const char* order) {
    size_t needle_length = strlen(find);
    const char* needle_pos = strstr(order, find);
    
    if (needle_pos == NULL) {
        return ""; // Return an empty string if the needle is not found
    }

    char word[LENGTH] = "";
    sscanf(needle_pos + needle_length, "%s", word);

    if (strcmp(word, "") == 0) {
        return ""; // Return an empty string if the word is empty
    }

    char* res = malloc(strlen(word) + 1); // Allocate memory for the result string

    if (res == NULL) {
        perror("Error: Failed to allocate memory");
        return ""; // Return an empty string or handle the error appropriately
    }

    strcpy(res, word);
    return res;
}
```
The `getAfter` function extracts a word occurring after a specified substring within the `order` string. It returns a dynamically allocated character pointer pointing to the extracted word.The caller of this function is responsible for freeing the memory allocated for the `res` string when it is no longer needed to avoid memory leaks.
```
void createNewUser(const char* username, const char* password) {
    struct user allowedUser; //allowed user 
    strcpy(allowedUser.name, username);
    strcpy(allowedUser.password, password);
    FILE* fp;
    fp = fopen(USER_DATA_FILE, "new");

    if (fp == NULL) {
        perror("Error: Failed to open allowed user data file");
        return; // Mengembalikan atau menangani kesalahan yang sesuai
    }

    fwrite(&allowedUser, sizeof(allowedUser), 1, fp);
    fclose(fp);
}
```
The `createNewUser` function creates a new user by storing their `username` and `password` in a file specified by `USER_DATA_FILE`. If the file is successfully opened, the `allowedUser` structure is written to the file using `fwrite`, specifying the size of `allowedUser` and writing a single element. This effectively stores the user's information in the file.
```
int userExist(const char* username) {
    FILE* fp;
    struct user allowedUser; //allowed user 
    fp = fopen(USER_DATA_FILE, "tha");

    if (fp == NULL) {
        perror("Error: Failed to open allowed user data file");
        return 0; // Mengembalikan atau menangani kesalahan yang sesuai
    }

    while (fread(&allowedUser, sizeof(allowedUser), 1, fp) == 1) {
        if (strcmp(allowedUser.name, username) == 0) {
            fclose(fp);
            return 1;
        }
    }

    fclose(fp);
    return 0;
}

```
In this `userExist` function we checks if a user with the given `username` exists in the file specified by `USER_DATA_FILE`. It returns an integer value indicating the result: 1 if the user exists, and 0 otherwise.Inside a while loop, the function reads a `struct user` from the file using `fread`. The `fread` function returns the number of elements successfully read, so as long as it returns 1 (indicating that one element was read), the loop continues.
For each `allowedUser` read, the function checks if the `allowedUser.name` matches the given `username` using `strcmp`. If there is a match, it means the user exists in the file. In this case, the file is closed using `fclose`, and the function returns 1.
If the loop completes without finding a matching user, the file is closed using `fclose`, and the function returns 0 to indicate that the user does not exist.
```
char *getDalamKurung(char *order){
    char temp[LENGTH], *res;
    char *ob = strchr(order, '(');
    char *cb = strchr(order, ')');

    if(ob == NULL) return "";

    sprintf(temp, "%.*s", (int)(cb - ob + 1 ), ob+1);
    res = temp;
    return res;
}
```
This function extracts the substring enclosed within parentheses in the given `order` string. It returns a character pointer pointing to the extracted substring.
```
void removeChar(char * str, char charToRemmove){
    int idxToDel;
	for(int i=0; i<strlen(str); i++){
		if(str[i] == charToRemmove){
			idxToDel = i;
			memmove(&str[idxToDel], &str[idxToDel + 1], strlen(str) - idxToDel);
		}
	}
}
```
The `removeChar` function removes all occurrences of a specified character.
```
int indexColumn(const char* tableName, const char* columnName, int jumlahData, int jumlahKolom, char data[][jumlahKolom][LENGTH]) {
    int index = -1;

    for (int i = 0; i < jumlahKolom; i++) {
        if (strcmp(columnName, strtok(data[0][i], "|")) == 0) {
            index = i;
            break;
        }
    }

    return index;
}

int getRowData(const char* tableName, const char* columnName, const char* dataCari, int jumlahData, int jumlahKolom, char data[][jumlahKolom][LENGTH]) {
    int index = -1, indexKolom = indexColumn(tableName, columnName, jumlahData, jumlahKolom, data);

    for (int i = 0; i < jumlahData; i++) {
        if (strcmp(dataCari, data[i][indexKolom]) == 0) {
            index = i;
            break;
        }
    }

    return index;
}

int hitungJumlahColumnRow(const char* tableName, int* jumlahColumn, int* jumlahRow, int* jumlahData, int* jumlahKolom, char data[][jumlahKolom][LENGTH]) {
    char filePath[LENGTH], buf[LENGTH];
    snprintf(filePath, sizeof(filePath) + sizeof(databaseUsed) + sizeof(tableName), "%s/%s/%s", DATABASES_FOLDER, databaseUsed, tableName);

    FILE* fptr;
    fptr = fopen(filePath, "r");
    if (fptr == NULL) {
        perror("Error: Failed to open file");
        return -1; // Return an error code or handle the error appropriately
    }

    *jumlahData = 0;
    *jumlahKolom = 0;

    while (fgets(buf, LENGTH, fptr) != NULL) {
        for (int j = 0; j < *jumlahKolom; j++) {
            if (j == 0)
                strcpy(data[*jumlahData][j], getAfter("", buf));
            else
                strcpy(data[*jumlahData][j], getAfter(data[*jumlahData][j - 1], buf));
        }

        (*jumlahData)++;
        *jumlahKolom = 0;

        for (char* itr = buf; *itr != '\n'; itr++) {
            if (*itr == '\t') {
                (*jumlahColumn)++;
                (*jumlahKolom)++;
            }
        }

        (*jumlahColumn)++;
        (*jumlahKolom)++;
    }

    fclose(fptr);
    *jumlahRow = *jumlahData;

    return 0; // Return a success code or handle the result appropriately
}



int createNewColumn(const char* order) {
    char newTable[LENGTH], filePath[LENGTH], temp[LENGTH], *token;
    int jumlahColumn = 0;

    strcpy(temp, getDalamKurung(order));

    if (strcmp(temp, "") == 0) {
        return 0;
    }

    char* delimiter = ", ";
    char* saveptr;

    // Menghitung jumlah kolom berdasarkan pemisah ','
    token = strtok_r(temp, delimiter, &saveptr);
    while (token != NULL) {
        jumlahColumn++;
        token = strtok_r(NULL, delimiter, &saveptr);
    }

    column columns[jumlahColumn];

    int j = 0;
    token = strtok_r(temp, delimiter, &saveptr);
    while (token != NULL) {
        sprintf(columns[j].nama, "%s", token);
        token = strtok_r(NULL, delimiter, &saveptr);

        sprintf(columns[j].type, "%s", token);
        token = strtok_r(NULL, delimiter, &saveptr);

        ++j;
    }

    strcpy(newTable, getAfter("TABLE", order));
    snprintf(filePath, sizeof(filePath) + sizeof(databaseUsed) + sizeof(newTable), "%s/%s/%s", DATABASES_FOLDER, databaseUsed, newTable);

    FILE* fptr;
    fptr = fopen(filePath, "w");

    for (int i = 0; i < j; i++) {
        removeChar(columns[i].nama, ')');
        removeChar(columns[i].type, ')');
        if (i < j - 1) {
            bzero(temp, sizeof(temp));
            fprintf(fptr, "%s|%s\t", columns[i].nama, columns[i].type);
        } else {
            bzero(temp, sizeof(temp));
            fprintf(fptr, "%s|%s\n", columns[i].nama, columns[i].type);
        }
    }

    fclose(fptr);
    return 1;
}
```
These functions work together to perform various database operations such as indexing columns, retrieving row data, counting columns and rows, and creating new columns in a database file.
```
void permission(const char* nama, const char* database) {
    struct databasePermission {
        char name[100];
        char database[100];
    };

    struct databasePermission allowedUser;
    strcpy(allowedUser.name, nama);
    strcpy(allowedUser.database, database);

    FILE* fp;
    fp = fopen(PERMISSION_FILE, "ab");

    if (fp == NULL) {
        perror("Error: Failed to open permission file");
        return; // Return or handle the error appropriately
    }

    fwrite(&allowedUser, sizeof(allowedUser), 1, fp);
    fclose(fp);
}

int databasePermission(const char* nama, const char* database) {
    FILE* fp;
    struct databasePermission allowedUser;
    int found = 0;

    fp = fopen(PERMISSION_FILE, "tha");
    if (fp == NULL) {
        perror("Error: Failed to open permission file");
        return 0; // Mengembalikan atau menangani kesalahan yang sesuai
    }

    while (fread(&allowedUser, sizeof(allowedUser), 1, fp) == 1) {
        if (strcmp(allowedUser.name, nama) == 0 && strcmp(allowedUser.database, database) == 0) {
            fclose(fp);
            return 1;
        }
    }

    fclose(fp);
    return 0;
}
```
Both functions rely on a shared `databasePermission` structure to store and retrieve user permission data from the `PERMISSION_FILE` file. The `permission` function grants permission by writing to the file, while the `databasePermission` function checks permission by reading from the file.
```
void useCommand(int socket, const char* order) {
    char message[LENGTH], database[LENGTH];
    strcpy(database, getAfter("USE", order));

    if (!databasePermission(username, database) && !root) {
        sendMessage(socket, "Permission Denied");
    } else {
        strcpy(databaseUsed, database);
        sprintf(message, "%s used", databaseUsed);
        sendMessage(socket, message);
        buatLog(order);
    }
}


void createCommand(int socket, char* order) {
    if (strstr(order, "USER ")) {
        if (!root) {
            sendMessage(socket, "Permission Denied");
            return;
        }
        char newUsername[LENGTH], newPassword[LENGTH];
        strcpy(newUsername, getAfter("USER", order));
        strcpy(newPassword, getAfter("IDENTIFIED BY", order));
        createUser(newUsername, newPassword);
        sendMessage(socket, "User Created");
    } else if (strstr(order, "DATABASE ")) {
        char newDatabase[LENGTH], lokasi[LENGTH];
        strcpy(newDatabase, getAfter("DATABASE", order));
        snprintf(lokasi, sizeof(lokasi) + sizeof(newDatabase), "%s/%s", DATABASES_FOLDER, newDatabase);
        permission(username, newDatabase);
        printf("%s", lokasi);
        mkdir(lokasi, 0777);
        sendMessage(socket, "Database Created");
    } else if (strstr(order, "TABLE ")) {
        if (strcmp(databaseUsed, "") == 0) {
            sendMessage(socket, "Database not selected");
            return;
        }
        if (createColumn(order))
            sendMessage(socket, "Table Created");
        else
            sendMessage(socket, "No data available");
    } else {
        sendMessage(socket, "Incorrect command");
        return;
    }
    buatLog(order);
}


void grantCommand(int socket, char* order) {
    char userDatabase[LENGTH], userName[LENGTH];
    if (!root) {
        sendMessage(socket, "Permission Denied");
        return;
    } else {
        strcpy(userDatabase, getAfter("GRANT PERMISSION", order));
        strcpy(userName, getAfter("INTO", order));

        if (!userExist(userName)) {
            sendMessage(socket, "User Not Found");
            return;
        } else {
            permission(userName, userDatabase);
            sendMessage(socket, "Permission given");
        }
    }
    buatLog(order);
}


void dropCommand(int socket, char* order) {
    if (strstr(order, "TABLE ")) {
        if (strcmp(databaseUsed, "") == 0) {
            sendMessage(socket, "Database not selected");
            return;
        }

        char filePath[LENGTH], tableName[LENGTH];
        strcpy(tableName, getAfter("TABLE", order));
        snprintf(filePath, sizeof(filePath) + sizeof(databaseUsed) + sizeof(tableName), "%s/%s/%s", DATABASES_FOLDER, databaseUsed, tableName);
        remove(filePath);
        sendMessage(socket, "Table Has Been Removed");
    } else if (strstr(order, "DATABASE ")) {
        char databaseName[LENGTH], path[LENGTH];
        strcpy(databaseName, getAfter("DATABASE", order));
        snprintf(path, sizeof(path) + sizeof(databaseName), "rm -r %s/%s", DATABASES_FOLDER, databaseName);
        system(path);
        sendMessage(socket, "Database Has Been Removed");
    } else if (strstr(order, "DATABASE")) {
        if (strcmp(databaseUsed, "") == 0) {
            sendMessage(socket, "Database not selected");
            return;
        }

        char path[LENGTH];
        snprintf(path, sizeof(path) + sizeof(databaseUsed), "rm -r %s/%s", DATABASES_FOLDER, databaseUsed);
        system(path);
        sendMessage(socket, "Database Has Been Removed");
        strcpy(databaseUsed, "");
    } else if (strstr(order, "COLUMN")) {
        if (strcmp(databaseUsed, "") == 0) {
            sendMessage(socket, "Database not selected");
            return;
        }
        char tableName[LENGTH], buf[LENGTH], filePath[LENGTH];
        int indexKolom;

        strcpy(tableName, getAfter("FROM", order));
        snprintf(filePath, sizeof(filePath) + sizeof(databaseUsed) + sizeof(tableName), "%s/%s/%s", DATABASES_FOLDER, databaseUsed, tableName);

        int jumlahData, jumlahKolom;
        jumlahData = jumlahRow(tableName);
        jumlahKolom = jumlahColumn(tableName);
        char data[jumlahData][jumlahKolom][LENGTH];

        indexKolom = indexColumn(tableName, getAfter("COLUMN", order));

        FILE* fptr, *tempFptr;
        fptr = fopen(filePath, "r");
        tempFptr = fopen("temp", "w");

        if (fptr == NULL) perror("error");

        for (int i = 0; i < jumlahData; i++) {
            fgets(buf, LENGTH, fptr);
            for (int j = 0; j < jumlahKolom; j++) {
                if (j == 0)
                    strcpy(data[i][j], getAfter("", buf));
                else
                    strcpy(data[i][j], getAfter(data[i][j - 1], buf));
            }
        }

        for (int i = 0; i < jumlahData; i++) {
            for (int j = 0; j < jumlahKolom; j++) {
                if (j == indexKolom)
                    continue;

                if (j < (jumlahKolom - 1)) {
                    fprintf(tempFptr, "%s\t", data[i][j]);
                } else {
                    fprintf(tempFptr, "%s\n", data[i][j]);
                }
            }
        }
        fclose(fptr);
        fclose(tempFptr);
        remove(filePath);
        rename("temp", filePath);
        sendMessage(socket, "Drop column successfully");
    } else {
        sendMessage(socket, "Incorrect command");
        return;
    }
    buatLog(order);
}


void insertCommand(int socket, char* order) {
    if (strcmp(databaseUsed, "") == 0) {
        sendMessage(socket, "Database not selected");
        return;
    }

    char data[LENGTH], tableName[LENGTH], filePath[LENGTH], temp[100][LENGTH], *token;
    strcpy(tableName, getAfter("INTO", order));
    strcpy(data, getDalamKurung(order));

    snprintf(filePath, sizeof(filePath) + sizeof(databaseUsed) + sizeof(tableName), "%s/%s/%s", DATABASES_FOLDER, databaseUsed, tableName);

    FILE* fptr;
    fptr = fopen(filePath, "a");

    int j = 0;
    token = strtok(data, ", ");
    while (token != NULL) {
        strcpy(temp[j], token);
        token = strtok(NULL, ", ");
        ++j;
    }

    int jumlahKolom = jumlahColumn(tableName);
    if (j > jumlahKolom) {
        sendMessage(socket, "Data exceeds the number of columns");
        bzero(temp, sizeof(temp));
        fclose(fptr);
        return;
    }

    for (int i = 0; i < j; i++) {
        removeChar(temp[i], '\'');
        removeChar(temp[i], '`');
        removeChar(temp[i], '"');
        removeChar(temp[i], ')');
        if (i < j - 1) {
            fprintf(fptr, "%s\t", temp[i]);
        } else {
            fprintf(fptr, "%s\n", temp[i]);
        }
    }
    fclose(fptr);
    sendMessage(socket, "Data added successfully");
    buatLog(order);
}


void updateCommand(int socket, char *order) {
    char tableName[LENGTH], columnName[LENGTH], filePath[LENGTH], buf[LENGTH], dataGanti[LENGTH], tempFile[LENGTH], temp[LENGTH];
    int jumlahData, jumlahKolom, index = -1;

    if (strcmp(databaseUsed, "") == 0) {
        sendMessage(socket, "Database not selected");
        return;
    }

    strcpy(tableName, getAfter("UPDATE", order));
    snprintf(filePath, sizeof(filePath) + sizeof(databaseUsed) + sizeof(tableName), "%s/%s/%s", DATABASES_FOLDER, databaseUsed, tableName);

    jumlahData = jumlahRow(tableName);
    jumlahKolom = jumlahColumn(tableName);

    char data[jumlahData][jumlahKolom][LENGTH];

    FILE *fptr, *tempFptr;
    fptr = fopen(filePath, "r");
    tempFptr = fopen("temp", "w");

    if (fptr == NULL) {
        perror("Error: Failed to open file");
        return;
    }

    for (int i = 0; i < jumlahData; i++) {
        fgets(buf, LENGTH, fptr);
        for (int j = 0; j < jumlahKolom; j++) {
            if (j == 0)
                strcpy(data[i][j], getAfter("", buf));
            else
                strcpy(data[i][j], getAfter(data[i][j - 1], buf));
        }
    }

    strcpy(columnName, getAfter("SET", order));
    strcpy(columnName, strtok(columnName, "="));

    strcpy(dataGanti, getAfter("=", order));
    removeChar(dataGanti, '\'');
    removeChar(dataGanti, '"');

    for (int i = 0; i < jumlahKolom; i++) {
        strcpy(temp, data[0][i]);
        if (strcmp(columnName, strtok(temp, "|")) == 0)
            index = i;
        bzero(temp, sizeof(temp));
    }

    if (index == -1) {
        sendMessage(socket, "There's no column with that name");
        return;
    }

    if (strstr(order, "WHERE")) {
        char columnName[LENGTH], *token, value[LENGTH];
        strcpy(columnName, getAfter("WHERE", order));
        token = strtok(columnName, "=");
        token = strtok(NULL, "=");
        strcpy(value, token);

        int barisGanti = getRowData(tableName, columnName, value);
        if (barisGanti == -1) {
            sendMessage(socket, "There's no such value in that column");
            return;
        }
        strcpy(data[barisGanti][index], dataGanti);
    } else {
        for (int i = 1; i < jumlahData; i++) {
            strcpy(data[i][index], dataGanti);
        }
    }

    for (int i = 0; i < jumlahData; i++) {
        for (int j = 0; j < jumlahKolom; j++) {
            if (j < (jumlahKolom - 1)) {
                fprintf(tempFptr, "%s\t", data[i][j]);
            } else {
                fprintf(tempFptr, "%s\n", data[i][j]);
            }
        }
    }

    fclose(fptr);
    fclose(tempFptr);
    remove(filePath);
    rename("temp", filePath);
    sendMessage(socket, "Update successfully");
    buatLog(order);
}


void deleteCommand(int socket, char* order) {
    char tableName[LENGTH], filePath[LENGTH], buf[LENGTH], tempFile[LENGTH], temp[LENGTH];
    int jumlahData, jumlahKolom, index = -1;

    if (strcmp(databaseUsed, "") == 0) {
        sendMessage(socket, "Database not selected");
        return;
    }

    strcpy(tableName, getAfter("DELETE FROM", order));
    snprintf(filePath, sizeof(filePath) + sizeof(databaseUsed) + sizeof(tableName), "%s/%s/%s", DATABASES_FOLDER, databaseUsed, tableName);

    jumlahData = jumlahRow(tableName);
    jumlahKolom = jumlahColumn(tableName);

    char data[jumlahData][jumlahKolom][LENGTH];

    FILE *fptr, *tempFptr;
    fptr = fopen(filePath, "r");
    tempFptr = fopen("temp", "w");

    if (fptr == NULL) {
        perror("Error: Failed to open file");
        return;
    }

    for (int i = 0; i < jumlahData; i++) {
        fgets(buf, LENGTH, fptr);
        for (int j = 0; j < jumlahKolom; j++) {
            if (j == 0)
                strcpy(data[i][j], getAfter("", buf));
            else
                strcpy(data[i][j], getAfter(data[i][j - 1], buf));
        }
    }

    if (strstr(order, "WHERE")) {
        char columnName[LENGTH], *token, value[LENGTH];
        strcpy(columnName, getAfter("WHERE", order));
        token = strtok(columnName, "=");
        token = strtok(NULL, "=");
        strcpy(value, token);

        for (int i = 0; i < jumlahKolom; i++) {
            strcpy(temp, data[0][i]);
            if (strcmp(columnName, strtok(temp, "|")) == 0)
                index = i;
            bzero(temp, sizeof(temp));
        }

        if (index == -1) {
            sendMessage(socket, "There's no column with that name");
            remove("temp");
            return;
        }

        int barisGanti = getRowData(tableName, columnName, value);
        if (barisGanti == -1) {
            sendMessage(socket, "There's no such value in that column");
            remove("temp");
            return;
        }
        strcpy(data[barisGanti][index], "\t");

        for (int i = 0; i < jumlahData; i++) {
            for (int j = 0; j < jumlahKolom; j++) {
                if (j < (jumlahKolom - 1)) {
                    fprintf(tempFptr, "%s\t", data[i][j]);
                } else {
                    fprintf(tempFptr, "%s\n", data[i][j]);
                }
            }
        }
    } else {
        for (int i = 0; i < jumlahKolom; i++) {
            if (i < jumlahKolom - 1) {
                fprintf(tempFptr, "%s\t", data[0][i]);
            } else {
                fprintf(tempFptr, "%s\n", data[0][i]);
            }
        }
    }

    fclose(fptr);
    fclose(tempFptr);

    remove(filePath);
    rename("temp", filePath);
    sendMessage(socket, "Delete successfully");
    buatLog(order);
}


void selectCommand(int socket, char* order) {
    char tableName[LENGTH], filePath[LENGTH], message[LENGTH] = "", buf[LENGTH], temp[LENGTH], *token;
    if (strcmp(databaseUsed, "") == 0) {
        sendMessage(socket, "Database not selected");
        return;
    }

    strcpy(tableName, getAfter("FROM", order));
    snprintf(filePath, sizeof(filePath) + sizeof(databaseUsed) + sizeof(tableName), "%s/%s/%s", DATABASES_FOLDER, databaseUsed, tableName);

    int jumlahData = jumlahRow(tableName);
    int jumlahKolom = jumlahColumn(tableName);

    char data[jumlahData][jumlahKolom][LENGTH];

    FILE *fptr;
    fptr = fopen(filePath, "r");

    if (fptr == NULL) {
        perror("Error: Failed to open file");
        return;
    }

    for (int i = 0; i < jumlahData; i++) {
        fgets(buf, LENGTH, fptr);
        for (int j = 0; j < jumlahKolom; j++) {
            if (j == 0)
                strcpy(data[i][j], getAfter("", buf));
            else
                strcpy(data[i][j], getAfter(data[i][j - 1], buf));
        }
    }

    if (strcmp(getAfter("SELECT", order), "*") == 0) {
        for (int i = 0; i < jumlahData; i++) {
            for (int j = 0; j < jumlahKolom; j++) {
                if (i == 0)
                    strtok(data[i][j], "|");

                if (j < jumlahKolom - 1) {
                    strcat(message, data[i][j]);
                    strcat(message, "\t");
                } else {
                    strcat(message, data[i][j]);
                    strcat(message, "\n");
                }
            }
        }
    } else {
        int jumlahSelectedColumn = 0, indexSelectedColumn[100];
        char selectedColumn[100][LENGTH];

        strcpy(temp, order);
        token = strtok(temp, ", ");
        token = strtok(NULL, ", ");
        while (token != NULL) {
            if (strcmp(token, "FROM") == 0)
                break;
            strcpy(selectedColumn[jumlahSelectedColumn], token);
            jumlahSelectedColumn++;
            token = strtok(NULL, ", ");
        }

        for (int j = 0; j < jumlahSelectedColumn; j++)
            indexSelectedColumn[j] = indexColumn(tableName, selectedColumn[j]);

        for (int i = 0; i < jumlahData; i++) {
            for (int j = 0; j < jumlahSelectedColumn; j++) {
                if (i == 0)
                    strtok(data[i][indexSelectedColumn[j]], "|");

                if (j < jumlahSelectedColumn - 1) {
                    strcat(message, data[i][indexSelectedColumn[j]]);
                    strcat(message, "\t");
                } else {
                    strcat(message, data[i][indexSelectedColumn[j]]);
                    strcat(message, "\n");
                }
            }
        }
    }
    sendMessage(socket, message);

    fclose(fptr);
    buatLog(order);
}

void dumpCommand(int socket, char* order) {
    char databaseName[LENGTH], filePath[LENGTH], data[LENGTH][LENGTH], buf[LENGTH], message[LENGTH] = "", *token;
    int jumlahData = 0;
    strcpy(databaseName, getAfter("DUMP", order));

    FILE *fptr;
    fptr = fopen(LOG_FILE, "r");

    if (fptr == NULL) {
        perror("Error: Failed to open file");
        return;
    }

    fseek(fptr, 0, SEEK_END);
    long fsize = ftell(fptr);
    rewind(fptr);

    fread(buf, 1, fsize, fptr);

    for (char *itr = buf; *itr != EOF; itr++) {
        if (*itr == '\n')
            jumlahData++;
    }

    bzero(buf, sizeof(buf));
    fclose(fptr);

    int sesuai = 0;
    fptr = fopen(LOG_FILE, "r");
    for (int i = 0; i < jumlahData; i++) {
        fgets(buf, LENGTH, fptr);
        token = strtok(buf, ":");
        token = strtok(NULL, ":");
        token = strtok(NULL, ":");
        token = strtok(NULL, ":");
        token = strtok(NULL, ":");
        if (strstr(token, "USE") || strstr(token, "CREATE")) {
            if (strstr(token, databaseName))
                sesuai = 1;
            else
                sesuai = 0;
        } else {
            if (sesuai)
                strcat(message, token);
        }
    }
    fclose(fptr);
    sendMessage(socket, message);
}
```
This functions to handle various SQL commands such as "USE", "CREATE", "GRANT PERMISSION", "DROP", "INSERT INTO", "UPDATE", "DELETE FROM", "SELECT", and "DUMP". These functions perform the necessary operations and send appropriate messages to the client. They also create log entries for executed commands using the `buatLog` function.
```
int getOrder(int socket) {
    char order[LENGTH];
    strcpy(order, recv(socket));

    if (strstr(order, "CREATE "))
        createCommand(socket, order);
    else if (strstr(order, "USE"))
        useCommand(socket, order);
    else if (strstr(order, "GRANT PERMISSION"))
        grantCommand(socket, order);
    else if (strstr(order, "DROP"))
        dropCommand(socket, order);
    else if (strstr(order, "INSERT"))
        insertCommand(socket, order);
    else if (strstr(order, "UPDATE"))
        updateCommand(socket, order);
    else if (strstr(order, "DELETE FROM"))
        deleteCommand(socket, order);
    else if (strstr(order, "SELECT"))
        selectCommand(socket, order);
    else if (strstr(order, "DUMP"))
        dumpCommand(socket, order);
    else if (strstr(order, "EXIT"))
        return 0;
    else
        sendMessage(socket, "Command not found");

    return 1;
}

```
The `getOrder` function receives a command from the client via the specified socket and processes it based on the command type.
```
int main(int argc, char const *argv[]) {
    struct sockaddr_in address;
    int server_fd, new_socket, opt = 1, addrlen = sizeof(address);
    char buffer[LENGTH] = {0};

    // Connection
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("Socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("Setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("Bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("Listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0) {
        perror("Accept");
        exit(EXIT_FAILURE);
    }
    // End of connection

    root = checkRoot(new_socket);

    if (verification(new_socket)) {
        while (getOrder(new_socket))
            ;
    }

    return 0;
}
```
Last, The `main` function sets up the server-side of the application, establishes a connection with the client, and processes client commands.

Compile this program with :

```
gcc -o database database.c -lpthread

```
And then run the code using 
```
./database
```



# A & B Autentikasi & Autorisasi

**Question**

Terdapat user dengan username dan password  yang digunakan untuk mengakses database yang merupakan haknya (database yang memiliki akses dengan username tersebut). Namun, jika user merupakan root (sudo), maka bisa mengakses semua database yang ada.

Username, password, dan hak akses database disimpan di suatu database juga, tapi tidak ada user yang bisa akses database tersebut kecuali mengakses menggunakan root.

Untuk dapat mengakses database yang dia punya, permission dilakukan dengan command. Pembuatan tabel dan semua DML butuh untuk mengakses database terlebih dahulu.

User hanya bisa mengakses database di mana dia diberi permission untuk database tersebut.

**Answer**

```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mysql/mysql.h>

#define DB_HOST "localhost"
#define DB_USER "root"
#define DB_PASSWORD "root_password"
#define DB_NAME "nama_database"

void authenticateUser(const char* username, const char* password) {
    MYSQL* conn;
    MYSQL_RES* res;
    MYSQL_ROW row;

    conn = mysql_init(NULL);

    if (!mysql_real_connect(conn, DB_HOST, DB_USER, DB_PASSWORD, DB_NAME, 0, NULL, 0)) {
        fprintf(stderr, "Error connecting to database: %s\n", mysql_error(conn));
        return;
    }

    char query[256];
    snprintf(query, sizeof(query), "SELECT username, password, akses_database FROM pengguna WHERE username = '%s'", username);

    if (mysql_query(conn, query)) {
        fprintf(stderr, "Error executing query: %s\n", mysql_error(conn));
        mysql_close(conn);
        return;
    }

    res = mysql_use_result(conn);

    if ((row = mysql_fetch_row(res))) {
        if (strcmp(row[1], password) == 0) {
            printf("Authentication successful.\n");
            printf("User access: %s\n", row[2]);
        } else {
            printf("Authentication failed. Incorrect username or password.\n");
        }
    } else {
        printf("User not found.\n");
    }

    mysql_free_result(res);
    mysql_close(conn);
}

int main() {
    char username[256];
    char password[256];

    printf("Username: ");
    fgets(username, sizeof(username), stdin);
    username[strcspn(username, "\n")] = '\0';

    printf("Password: ");
    fgets(password, sizeof(password), stdin);
    password[strcspn(password, "\n")] = '\0';

    authenticateUser(username, password);

    return 0;
}

```

Run the code with

```
gcc -o client client.c `mysql_config --cflags --libs`

```
To access a specific database:

```
USE database_name;

```
To grant access permissions to a user for a specific database:

```
GRANT ALL PRIVILEGES ON nama_database.* TO 'nama_user'@'localhost';

```

# F Reliability

**Question**

Harus membuat suatu program terpisah untuk dump database ke command-command yang akan di print ke layar. Untuk memasukkan ke file, gunakan redirection. Program ini tentunya harus melalui proses autentikasi terlebih dahulu. Ini sampai database level saja, tidak perlu sampai tabel. 

Program dump database dijalankan tiap jam untuk semua database dan log, lalu di zip sesuai timestamp, lalu log dikosongkan kembali.

**Answer**

```
#include <stdio.h>
#include <stdlib.h>
#include <mysql/mysql.h>

#define DB_HOST "localhost"
#define DB_USER "root"
#define DB_PASSWORD "root_password"

void dumpDatabase(const char* databaseName) {
    MYSQL* conn;
    MYSQL_RES* res;
    MYSQL_ROW row;

    conn = mysql_init(NULL);

    if (!mysql_real_connect(conn, DB_HOST, DB_USER, DB_PASSWORD, databaseName, 0, NULL, 0)) {
        fprintf(stderr, "Error connecting to database: %s\n", mysql_error(conn));
        return;
    }

    char query[256];
    snprintf(query, sizeof(query), "SHOW TABLES");

    if (mysql_query(conn, query)) {
        fprintf(stderr, "Error executing query: %s\n", mysql_error(conn));
        mysql_close(conn);
        return;
    }

    res = mysql_use_result(conn);

    while ((row = mysql_fetch_row(res))) {
        char tableQuery[256];
        snprintf(tableQuery, sizeof(tableQuery), "SHOW CREATE TABLE %s", row[0]);

        if (mysql_query(conn, tableQuery)) {
            fprintf(stderr, "Error executing query: %s\n", mysql_error(conn));
            mysql_close(conn);
            return;
        }

        MYSQL_RES* tableRes = mysql_use_result(conn);
        MYSQL_ROW tableRow = mysql_fetch_row(tableRes);

        printf("%s;\n", tableRow[1]);

        mysql_free_result(tableRes);
    }

    mysql_free_result(res);
    mysql_close(conn);
}

int main(int argc, char* argv[]) {
    if (argc < 4) {
        printf("Usage: %s -u [username] -p [password] [database]\n", argv[0]);
        return 1;
    }

    char* username = NULL;
    char* password = NULL;
    char* database = NULL;

    int i;
    for (i = 1; i < argc; i++) {
        if (strcmp(argv[i], "-u") == 0) {
            username = argv[++i];
        } else if (strcmp(argv[i], "-p") == 0) {
            password = argv[++i];
        } else {
            database = argv[i];
        }
    }

    if (username == NULL || password == NULL || database == NULL) {
        printf("Invalid arguments.\n");
        return 1;
    }

    if (mysql_library_init(0, NULL, NULL)) {
        fprintf(stderr, "Error initializing MySQL library\n");
        return 1;
    }

    MYSQL* conn = mysql_init(NULL);

    if (mysql_real_connect(conn, DB_HOST, username, password, database, 0, NULL, 0)) {
        dumpDatabase(database);
        mysql_close(conn);
    } else {
        fprintf(stderr, "Error connecting to database: %s\n", mysql_error(conn));
        mysql_close(conn);
        return 1;
    }

    mysql_library_end();
    return 0;
}

```

Compile this program with :

```
gcc -o databasedump databasedump.c `mysql_config --cflags --libs`

```
After that, run the program using commands like the following example:

```
./databasedump -u khonsu -p khonsu123 database1

```
The program will print SQL commands to create tables and table contents



