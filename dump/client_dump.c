#include <stdio.h>
#include <stdlib.h>
#include <mysql/mysql.h>

#define DB_HOST "localhost"
#define DB_USER "root"
#define DB_PASSWORD "root_password"

void dumpDatabase(const char* databaseName) {
    MYSQL* conn;
    MYSQL_RES* res;
    MYSQL_ROW row;

    conn = mysql_init(NULL);

    if (!mysql_real_connect(conn, DB_HOST, DB_USER, DB_PASSWORD, databaseName, 0, NULL, 0)) {
        fprintf(stderr, "Error connecting to database: %s\n", mysql_error(conn));
        return;
    }

    char query[256];
    snprintf(query, sizeof(query), "SHOW TABLES");

    if (mysql_query(conn, query)) {
        fprintf(stderr, "Error executing query: %s\n", mysql_error(conn));
        mysql_close(conn);
        return;
    }

    res = mysql_use_result(conn);

    while ((row = mysql_fetch_row(res))) {
        char tableQuery[256];
        snprintf(tableQuery, sizeof(tableQuery), "SHOW CREATE TABLE %s", row[0]);

        if (mysql_query(conn, tableQuery)) {
            fprintf(stderr, "Error executing query: %s\n", mysql_error(conn));
            mysql_close(conn);
            return;
        }

        MYSQL_RES* tableRes = mysql_use_result(conn);
        MYSQL_ROW tableRow = mysql_fetch_row(tableRes);

        printf("%s;\n", tableRow[1]);

        mysql_free_result(tableRes);
    }

    mysql_free_result(res);
    mysql_close(conn);
}

int main(int argc, char* argv[]) {
    if (argc < 4) {
        printf("Usage: %s -u [username] -p [password] [database]\n", argv[0]);
        return 1;
    }

    char* username = NULL;
    char* password = NULL;
    char* database = NULL;

    int i;
    for (i = 1; i < argc; i++) {
        if (strcmp(argv[i], "-u") == 0) {
            username = argv[++i];
        } else if (strcmp(argv[i], "-p") == 0) {
            password = argv[++i];
        } else {
            database = argv[i];
        }
    }

    if (username == NULL || password == NULL || database == NULL) {
        printf("Invalid arguments.\n");
        return 1;
    }

    if (mysql_library_init(0, NULL, NULL)) {
        fprintf(stderr, "Error initializing MySQL library\n");
        return 1;
    }

    MYSQL* conn = mysql_init(NULL);

    if (mysql_real_connect(conn, DB_HOST, username, password, database, 0, NULL, 0)) {
        dumpDatabase(database);
        mysql_close(conn);
    } else {
        fprintf(stderr, "Error connecting to database: %s\n", mysql_error(conn));
        mysql_close(conn);
        return 1;
    }

    mysql_library_end();
    return 0;
}

